<?php

/**
 * Permet d'ajouter des custom fields en lien
 * avec les customs posts Apprenants
 *
 * @since      1.0.0
 * @package    Api_edn
 * @subpackage Api-edn/includes
 * @author     Thomas Peresson <thomas.peresson@gmail.com>
 */

function api_edn_render_metaboxes($post_id)
{
    
    $firstNname = !false ? get_post_meta($post_id->ID, 'api-edn_prenom_apprenant', true) : false;
    $lastNname = !false ? get_post_meta($post_id->ID, 'api-edn_nom_apprenant', true) : false;
    $linkedinURL = !false ? get_post_meta($post_id->ID, 'api-edn_linkedin_apprenant', true) : false;
    $portfolioURL = !false ? get_post_meta($post_id->ID, 'api-edn_url_portfolio_apprenant', true) : false;
    $cvURL = !false ? get_post_meta($post_id->ID, 'api-edn_url_cv_apprenant', true) : false;
?>
    <div class="api-edn-container">
        <p>* Tous les champs sont obligatoires</p>
        <div class="form-group">
            <label for="prenom_apprenant">Prénom <span class="required-star">*</span></label>
            <input type="text" name="api-edn_prenom_apprenant" id="prenom_apprenent" value="<?php echo $firstNname ?>" placeholder="Entrez son prénom ici" required>
        </div>
        <div class="form-group">
            <label for="nom_apprenant">Nom <span class="required-star">*</span></label>
            <input type="text" name="api-edn_nom_apprenant" id="nom_apprenent" value="<?php echo $lastNname ?>" placeholder="Entrez son nom ici" required>
        </div>
        <div class="form-group">
            <label for="linkedin_apprenent">Lien LinkedIn <span class="required-star">*</span></label>
            <input type="url" name="api-edn_linkedin_apprenant" id="linkedin_apprenent" value="<?php echo $linkedinURL ?>" placeholder="https://linkedin.com/in/..." required>
        </div>
        <div class="form-group">
            <label for="url_portfolio_apprenant">URL Portfolio <span class="required-star">*</span></label>
            <input type="url" name="api-edn_url_portfolio_apprenant" id="url_portfolio_apprenant" value="<?php echo $portfolioURL ?>" placeholder="https://site-apprenant.com/" required>
        </div>
        <div class="form-group">
            <label for="url_cv_apprenant">Lien téléchargement CV <span class="required-star">*</span></label>
            <input type="url" name="api-edn_url_cv_apprenant" id="url_cv_apprenent" value="<?php echo $cvURL ?>" placeholder="https://" required>
        </div>
    </div>
<?php
}

function api_edn_save_metaboxes($postID)
{
    if (
        array_key_exists('api-edn_nom_apprenant', $_POST) &&
        array_key_exists('api-edn_prenom_apprenant', $_POST) &&
        array_key_exists('api-edn_linkedin_apprenant', $_POST) &&
        array_key_exists('api-edn_url_portfolio_apprenant', $_POST) &&
        array_key_exists('api-edn_url_cv_apprenant', $_POST) &&
        current_user_can('edit_post', $postID)
    ) {
        // Champ Prénom
        if (empty($_POST['api-edn_prenom_apprenant'])) {
            delete_post_meta($postID, 'api-edn_prenom_apprenant');
        } else {
            update_post_meta($postID, 'api-edn_prenom_apprenant', $_POST['api-edn_prenom_apprenant']);
        }

        // Champ Nom
        if (empty($_POST['api-edn_nom_apprenant'])) {
            delete_post_meta($postID, 'api-edn_nom_apprenant');
        } else {
            update_post_meta($postID, 'api-edn_nom_apprenant', $_POST['api-edn_nom_apprenant']);
        }

        // Champ Lien LinkedIn
        if (empty($_POST['api-edn_linkedin_apprenant'])) {
            delete_post_meta($postID, 'api-edn_linkedin_apprenant');
        } else {
            update_post_meta($postID, 'api-edn_linkedin_apprenant', $_POST['api-edn_linkedin_apprenant']);
        }

        // Champ URL Portfolio
        if (empty($_POST['api-edn_url_portfolio_apprenant'])) {
            delete_post_meta($postID, 'api-edn_url_portfolio_apprenant');
        } else {
            update_post_meta($postID, 'api-edn_url_portfolio_apprenant', $_POST['api-edn_url_portfolio_apprenant']);
        }

        // Champ URL CV
        if (empty($_POST['api-edn_url_cv_apprenant'])) {
            delete_post_meta($postID, 'api-edn_url_cv_apprenant');
        } else {
            update_post_meta($postID, 'api-edn_url_cv_apprenant', $_POST['api-edn_url_cv_apprenant']);
        }
    }
}

function api_edn_create_custom_meta_box()
{
    add_meta_box('infos_apprenant', __('Infos Apprenant'), 'api_edn_render_metaboxes', 'apprenants', 'side');
}

function api_edn_add_custom_meta_boxes()
{
    add_action('add_meta_boxes', 'api_edn_create_custom_meta_box');
    add_action('save_post', 'api_edn_save_metaboxes');
}
