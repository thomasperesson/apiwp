<?php

/**
 * @link              https://thomas-peresson.com
 * @since             1.0.0
 * @package           Api_edn
 *
 * @wordpress-plugin
 * Plugin Name:       API EDN
 * Description:       Met en place l'API pour récupérer les données sur les CPT et certaines
 * Version:           1.0.0
 * Author:            Thomas Peresson
 * Text Domain:       api_edn
 * Domain Path:       /languages
 */

define('API_EDN_VERSION', '1.0.0');

require plugin_dir_path(__FILE__) . '/includes/api-edn_create_custom_post_and_taxes.php';
api_edn_add_apprenant_post_and_taxes();

require plugin_dir_path(__FILE__) . '/includes/api-edn_create_custom_fileds.php';
api_edn_add_custom_meta_boxes();

// function api_edn_activate()
// {
//     api_edn_create_custom_post_type();
//     flush_rewrite_rules();
// }
// register_activation_hook(__FILE__, 'api_edn_activate');

require plugin_dir_path(__FILE__) . '/includes/class-api-edn_activator.php';
Api_edn_Activator::activate();

require plugin_dir_path(__FILE__) . '/includes/class-api-edn_deactivator.php';
Api_edn_Deactivator::deactivate();

require plugin_dir_path(__FILE__) . '/includes/api-edn_api_routes.php';




/**
 * Permet de récupérer les données de la table posts de WP
 * 
 * @param string $postType
 * @return array $results
 */
function get_posts_infos($postType)
{
    /**
     * @var wpdb $wpdb
     */
    global $wpdb;
    $query = $wpdb->prepare("SELECT * FROM $wpdb->posts WHERE post_type=%s", [$postType]);
    $results = $wpdb->get_results($query, ARRAY_A);
    return $results;
}


/**
 * Ajoute une vérification de connexion de l'utilisateur avant
 * d'utiliser l'API Rest de WordPress
 */
add_filter('rest_authentication_errors', function ($result) {
    // If a previous authentication check was applied,
    // pass that result along without modification.
    if (true === $result || is_wp_error($result)) {
        return $result;
    }
    // No authentication has been performed yet.
    // Return an error if user is not logged in.
    if (!is_user_logged_in()) {
        return new WP_Error(
            'rest_not_logged_in',
            __('You are not currently logged in.'),
            array('status' => 401)
        );
    }

    // Our custom authentication check should have no effect
    // on logged-in requests
    return $result;
});

add_filter('rest_authentication_errors', function ($result) {
    /**
     * @var WP $wp
     */
    global $wp;
    if (false !== strpos($wp->query_vars['rest_route'], 'apiedn/v1')) {
        return true;
    }
    return $result;
}, 9);

function api_edn_register_scripts()
{
    wp_register_style('style', plugins_url('includes/css/style.css', __FILE__));
    wp_enqueue_style('style');
}

// Add action rest_api_init, api_edn_get_infos_apprenants
api_edn_add_routes();
add_action('admin_init', 'api_edn_register_scripts');
