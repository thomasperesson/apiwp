<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Api_edn
 * @subpackage Api-edn/includes
 * @author     Thomas Peresson <thomas.peresson@gmail.com>
 */
class Api_edn_Deactivator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {
        function api_edn_deactivate()
        {
            unregister_post_type('apprenants');
            flush_rewrite_rules();
        }
        register_deactivation_hook(__FILE__, 'api_edn_deactivate');
    }
}
