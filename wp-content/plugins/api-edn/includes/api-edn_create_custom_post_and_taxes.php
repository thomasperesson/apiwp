<?php

/**
 * Permet de créer le post type Apprenants ainsi
 * que les taxons associés
 *
 * @since      1.0.0
 * @package    Api_edn
 * @subpackage Api-edn/includes
 * @author     Thomas Peresson <thomas.peresson@gmail.com>
 */
function api_edn_create_apprenant_post_type()
{
    $labels = [
        'name'                  => __('Apprenants'),
        'singular_name'         => __('Apprenant'),
        'menu_name'             => __('Apprenants'),
        'parent_item_colon'     => __('Apprenant parent'),
        'all_items'             => __('Tous les apprenants'),
        'view_item'             => __('Voir l\'apprenant'),
        'add_new_item'          => __('Ajouter un nouvel apprenant'),
        'add_new'               => __('Ajouter'),
        'edit_item'             => __('Modifier l\'apprenant'),
        'update_item'           => __('Mettre à jour l\'apprenant'),
        'search_items'          => __('Rechercher un apprenant'),
        'not_found'             => __('Apprenant non trouvé'),
        'not_found_in_trash'    => __('Apprenant non trouvé dans la corbeille'),
    ];
    $args = array(
        'label'               => __('apprenants'),
        'description'         => __('Apprenants de la formations de l\'École du Numérique : Développeur Web et Web mobile'),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => ['title', 'excerpt', 'thumbnail', 'revisions'],
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_nav_menus'     => true,
        'show_in_admin_bar'     => true,
        'menu_position'         => 5,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
        'menu_icon'             => 'dashicons-businessperson',

    );
    register_post_type('apprenants', $args);
}

/**
 * Création d'un Taxon 'Promotions'
 * 
 * @return void
 */
function api_edn_create_taxes_promotion()
{
    /**
     * Taxonomy: Promotions.
     */

    $labels = [
        "name" => __("Promotions"),
        "singular_name" => __("Promotion"),
        "menu_name" => __("Promotions"),
        "all_items" => __("Promotions"),
        "edit_item" => __("Modifier Promotion"),
        "view_item" => __("Voir Promotion"),
        "update_item" => __("Mettre à jour le nom de la Promotion"),
        "add_new_item" => __("Ajouter une nouvelle Promotion"),
        "new_item_name" => __("Nom de la nouvelle Promotion"),
        "parent_item" => __("Parent de Promotion"),
        "parent_item_colon" => __("Parent Promotion :"),
        "search_items" => __("Recherche de Promotions"),
        "popular_items" => __("Promotions populaires"),
        "separate_items_with_commas" => __("Séparer les Promotions avec des virgules"),
        "add_or_remove_items" => __("Ajouter ou supprimer des Promotions"),
        "choose_from_most_used" => __("Choisir parmi les Promotions les plus utilisées"),
        "not_found" => __("Aucunes Promotions trouvées"),
        "no_terms" => __("Aucunes Promotions"),
        "items_list_navigation" => __("Navigation de liste de Promotions"),
        "items_list" => __("Liste des Promotions"),
        "back_to_items" => __("Retourner à Promotions"),
    ];


    $args = [
        "label" => __("Promotions"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => ['slug' => 'promotion', 'with_front' => true,],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "show_tagcloud" => false,
        "rest_base" => "promotion",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
        "show_in_graphql" => false,
    ];
    register_taxonomy("promotion", ["apprenants"], $args);
}

/**
 * Création d'un Taxon 'Compétences'
 * 
 * @return void
 */
function api_edn_create_taxes_competences()
{

    /**
     * Taxonomy: Compétences.
     */

    $labels = [
        "name" => __("Compétences"),
        "singular_name" => __("Compétence"),
        "menu_name" => __("Compétences"),
        "all_items" => __("Toutes les Compétences"),
        "edit_item" => __("Modifier Compétence"),
        "view_item" => __("Voir Compétence"),
        "update_item" => __("Mettre à jour le nom de Compétence"),
        "add_new_item" => __("Ajouter une nouvelle Compétence"),
        "new_item_name" => __("Nom de la nouvelle Compétence"),
        "parent_item" => __("Parent de la Compétence"),
        "parent_item_colon" => __("Parent Compétence :"),
        "search_items" => __("Recherche des Compétences"),
        "popular_items" => __("Compétences populaires"),
        "separate_items_with_commas" => __("Séparer les Compétences avec des virgules"),
        "add_or_remove_items" => __("Ajouter ou supprimer des Compétences"),
        "choose_from_most_used" => __("Choisir parmi les Compétences les plus utilisés"),
        "not_found" => __("Aucunes Compétences trouvées"),
        "no_terms" => __("Aucunes Compétences"),
        "items_list_navigation" => __("Navigation de liste de Compétences"),
        "items_list" => __("Liste des Compétences"),
        "back_to_items" => __("Retourner aux Compétences"),
    ];

    $args = [
        "label" => __("Compétences"),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => ['slug' => 'competences', 'with_front' => true,],
        "show_admin_column" => false,
        "show_in_rest" => true,
        "show_tagcloud" => false,
        "rest_base" => "competences",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
        "show_in_graphql" => false,
    ];
    register_taxonomy("competences", ["apprenants"], $args);
}


function api_edn_add_apprenant_post_and_taxes()
{
    add_action('init', 'api_edn_create_apprenant_post_type', 0);
    add_action('init', 'api_edn_create_taxes_promotion');
    add_action('init', 'api_edn_create_taxes_competences');
}
