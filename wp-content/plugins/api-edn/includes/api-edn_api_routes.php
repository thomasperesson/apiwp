<?php

/**
 * Permet d'ajouter des custom fields en lien
 * avec les customs posts Apprenants
 *
 * @since      1.0.0
 * @package    Api_edn
 * @subpackage Api-edn/includes
 * @author     Thomas Peresson <thomas.peresson@gmail.com>
 */


/**
 * Fonction callback
 * Récupère les metadatas
 */
function api_edn_get_infos_apprenants_callback()
{
    $data = [];
    $args = [
        'post_type' => 'apprenants',
        'postnumber' => -1
    ];


    // Get the posts using the 'post' and 'news' post types
    $posts = get_posts($args);

    // Loop through the posts and push the desired data to the array we've initialized earlier in the form of an object
    foreach ($posts as $post) {
        $id = $post->ID;
        $post_thumbnail = (has_post_thumbnail($id)) ? get_the_post_thumbnail_url($id) : null;

        $data[] = (object) array(
            'id' => $id,
            'prenom' => get_post_meta($id, 'api-edn_prenom_apprenant', true),
            'nom' => get_post_meta($id, 'api-edn_nom_apprenant', true),
            'linkedin' => get_post_meta($id, 'api-edn_linkedin_apprenant', true),
            'extrait' => $post->post_excerpt,
            'image' => $post_thumbnail,
            'portfolio' => get_post_meta($id, 'api-edn_url_portfolio_apprenant', true),
            'cv' => get_post_meta($id, 'api-edn_url_cv_apprenant', true),
            'promotion' => get_the_terms($id, 'promotion', true),
            'competences' => get_the_terms($id, 'competences'),
        );
    }
    return $data;
}

/**
 * Ajoute route pour les infos de tous les apprenants
 */
function api_edn_get_infos_apprenants()
{
    register_rest_route('apiedn/v1', '/apprenants/all', array(
        'methods' => 'GET',
        'callback' => 'api_edn_get_infos_apprenants_callback'
    ));
}

function api_edn_add_routes()
{
    // add_filter('rest_prepare_apprenants', 'api_edn_get_meta_data', 10, 2);
    add_action('rest_api_init', 'api_edn_get_infos_apprenants');
}
