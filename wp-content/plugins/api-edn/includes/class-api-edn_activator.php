<?php

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Api_edn
 * @subpackage Api-edn/includes
 * @author     Thomas Peresson <thomas.peresson@gmail.com>
 */
class Api_edn_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        function api_edn_activate()
        {
            require plugin_dir_path(__FILE__) . '/api-edn_create_custom_post_and_taxes.php';
            api_edn_add_apprenant_post_and_taxes();
            flush_rewrite_rules();
        }
        register_activation_hook(__FILE__, 'api_edn_activate');
    }
}
